package sort;

import sort.strategies.BubbleSort;

import java.util.Random;

public class PorownywarkaAlgorytmow {
    // maksymalna wartość losowanych liczb
    private final static int PROG_WARTOSCI = 1000;

    // strategia sortowania
    private AbstractSortStrategy strategia;
    // tablica do posortowania
    private Integer[] array;

    public PorownywarkaAlgorytmow() {
        losujLiczby(100);
        setStrategia(new BubbleSort());
    }

    /**
     * Ustawiam strategie i przekazuję jej referencję do tablicy do posortowania.
     *
     * @param strategia - strategia do ustawienia
     */
    public void setStrategia(AbstractSortStrategy strategia) {
        this.strategia = strategia;
        if (this.strategia != null) {
            this.strategia.setArray(array);
        }
    }

    /**
     * Losuje liczby z zakresu do PROG_WARTOSCI
     *
     * @param iloscLiczb - ilosc liczb do wylosowania
     */
    public void losujLiczby(int iloscLiczb) {
        array = new Integer[iloscLiczb];
        for (int i = 0; i < iloscLiczb; i++) {
            array[i] = new Random().nextInt(PROG_WARTOSCI);
        }
    }

    /**
     * Wywoluje sortowanie.
     */
    public void sortuj() {
        if (strategia == null) {
            throw new IllegalStateException("Ustaw strategie sortowania");
        }
        strategia.sort();
    }

    /**
     * Wywoluje sortowanie z mierzeniem czasu.
     */
    public void sortAndTime() {
        if (strategia == null) {
            throw new IllegalStateException("Ustaw strategie sortowania");
        }
        strategia.sortAndMeasureTime();
    }

    /**
     * Wypisuje tablice w strategii.
     */
    public void print() {
        if (strategia == null) {
            throw new IllegalStateException("Ustaw strategie sortowania");
        }
        strategia.print();
    }
}
